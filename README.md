# Pre-processing MP2RAGE data for high-res Freesurfer recon-all

<img src ="/workflow_figure/chapter4_figure1.jpg"/>

# Reference
For full thesis, see here: [Laminar fMRI at ultra-high fields: Acquisition and analysis strategies](https://gitlab.com/skash/mythesis2019/blob/master/Kashyap_Thesis_2019_print.pdf)  
This workflow is described in Chapter 4. 

Refer as:  
Kashyap, S. (2019). Laminar fMRI using arterial spin labelling. In *Laminar fMRI at ultra-high fields: Acquisition and analysis strategies* (pg. 127). doi:10.26481/dis.20190829sk


TO-DO:
Include thresholding and masking scripts as examples.